oldNumber = 1
currentNumber = 1
sum = 0

while currentNumber < 4000000:  
    temp = oldNumber + currentNumber
    if temp % 2 == 0:
        sum += temp
    oldNumber = currentNumber
    currentNumber = temp

print "total: %d" % (sum)
