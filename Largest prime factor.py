number = 600851475143
c = 2
while c**2 < number:
    if number % c == 0:
        number /= c
    c += 1
print(number)
